## Easy online forms tool

### We help you to design your own online forms such as application forms, registration forms, order forms and surveys.

Looking for online forms tool? We can design and place these forms on your internet or intranet site, or use them for e-mails or social media.

#### Our features:

* A/B Testing
* Form Conversion
* Form Optimization
* Branch logic
* Payment integration
* Third party integration
* Push notifications
* Multiple language support
* Conditional logic
* Validation rules
* Server rules
* Custom reports

### Designing forms can be done online because we have web-based online tool

We have all of the functions you would expect from a professional [online forms tool](https://formtitan.com).

Happy online forms tool!